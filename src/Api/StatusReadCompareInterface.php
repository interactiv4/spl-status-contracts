<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Status\Api;

/**
 * Interface StatusReadCompareInterface.
 *
 * Read / compare current status.
 * Most of times, an implementation providing StatusReadInterface capabilities, is also able to perform the adequate
 * comparison and provide StatusCompareInterface capabilities by using the retrieved value.
 *
 * This is a convenience interface to avoid implementors having to implement separately both interfaces:
 *
 * @see StatusReadInterface
 * @see StatusCompareInterface
 * @see StatusReadCompareTrait to help yourself to implement part of the functionality.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Status
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface StatusReadCompareInterface extends
    StatusReadInterface,
    StatusCompareInterface
{
}
