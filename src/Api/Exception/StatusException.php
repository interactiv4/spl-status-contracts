<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Status\Api\Exception;

use Exception;

/**
 * Class StatusException.
 *
 * All exceptions raised by status interfaces MUST be either:
 * - Subclasses of this class
 * - @see \RuntimeException or its subclasses
 *
 * For @see \LogicException or more specific exceptions, wrap them as previous into appropriate exception type:
 * @see CouldNotCompareStatusException
 * @see CouldNotPutStatusException
 * @see CouldNotReadStatusException
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Status
 */
abstract class StatusException extends Exception
{
}
