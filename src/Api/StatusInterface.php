<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Status\Api;

/**
 * Interface StatusInterface.
 *
 * Read / put / compare current status.
 *
 * This is a convenience interface intended for implementors that provide read / put / compare status capabilities.
 *
 * @see StatusReadInterface
 * @see StatusCompareInterface
 * @see StatusPutInterface
 * @see StatusReadCompareTrait to help yourself to implement part of the functionality.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Status
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface StatusInterface extends
    StatusReadInterface,
    StatusCompareInterface,
    StatusPutInterface
{
}
