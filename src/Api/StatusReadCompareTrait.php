<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Status\Api;

use Interactiv4\Contracts\SPL\Exception\Wrapper\Api\ExceptionWrapperTrait;
use Interactiv4\Contracts\SPL\Status\Api\Exception\CouldNotCompareStatusException;
use Interactiv4\Contracts\SPL\Status\Api\Exception\CouldNotReadStatusException;

/**
 * Trait StatusReadCompareTrait.
 *
 * For classes implementing both StatusReadInterface and StatusCompareInterface:
 * - Directly
 * - By using StatusReadCompareInterface
 * - By using StatusInterface
 *
 * isStatus() method can be implemented in a generic way. Php does not allow generic method bodies in interfaces yet,
 * so you can help yourself by using this trait to implement functionality in previously described cases.
 *
 * @see StatusReadInterface
 * @see StatusCompareInterface
 * @see StatusReadCompareInterface
 * @see StatusInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Status
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
trait StatusReadCompareTrait
{
    use ExceptionWrapperTrait;

    /**
     * {@inheritdoc}
     */
    public function isStatus(
        $status,
        array $context = [],
        bool $strict = false
    ): bool {
        /** @var StatusReadInterface|StatusReadCompareInterface|StatusInterface $this */
        try {
            $currentStatus = $this->getStatus($context);
        } catch (CouldNotReadStatusException $e) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(CouldNotCompareStatusException::class, $e);
        }

        /** @noinspection TypeUnsafeComparisonInspection */
        return $strict
            ? $status === $currentStatus
            : $status == $currentStatus;
    }
}
