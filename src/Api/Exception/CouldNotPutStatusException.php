<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Status\Api\Exception;

/**
 * Class CouldNotPutStatusException.
 *
 * Thrown when a non-runtime error occurs while putting status.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Status
 */
final class CouldNotPutStatusException extends StatusException
{
}
