<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Status\Api;

use Interactiv4\Contracts\SPL\Status\Api\Exception\CouldNotReadStatusException;
use LogicException;
use RuntimeException;

/**
 * Interface StatusReadInterface.
 *
 * Read current status.
 * Supports multiple status types.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Status
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface StatusReadInterface
{
    /**
     * Read current status.
     * It MAY use optionally supplied context to determine how status should be retrieved.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param array $context Optional, additional data to determine how status should be retrieved.
     *
     * @return mixed Current status. Avoid returning void / null. It SHOULD return something interpretable as a status.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when reading status.
     *
     * @throws CouldNotReadStatusException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function getStatus(array $context = []);
}
